from flask import Flask
from flask_restful import Resource, Api
import detect
import numpy as np
app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
    def get(self):
        n=detect.detect()
        return n
    
api.add_resource(HelloWorld, '/yolo')

if __name__ == '__main__':
    
    app.run(debug=True)